const ClienteModel = require("../models/cliente")

//Conexión con la BD para realizar las pruebas:
const conexionBD = require("../config/database")
//Ejecutar la función conexión
conexionBD()

const cliente1 = {
    tipoDocumento: "CC",
    numDocumento: "125",
    nombre: "PEPE3",
    apellido: "ALVAREZ",
    email: "pepealv@email.com",
    telefonoContacto: "3508024101",
    password: "0123"
}
//Los métodos CRUD se ejecutan como Promesas
ClienteModel.create(cliente1)
    .then( (respuesta) => {
        console.log(`Cliente ${respuesta.nombre} añadido con éxito`)
    } )
    .catch( () => {
        console.log(`Ocurrió un error al añadir al Cliente ${respuesta.nombre}`)
    } )