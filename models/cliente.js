//Realizar todo en un solo script se vuelve insostenible, por lo tanto, se crea una carpeta models para la creación de los respectivos modelos

const {Schema, model} = require("mongoose")

//1. Crear Esquema
const clienteSchema = new Schema({
    tipoDocumento: String,
    numDocumento: String,
    nombre: String,
    apellido: String,
    email: String,
    telefonoContacto: String,
    password: String
})

//2. Crear Modelo -> CRUD = Create - Read - Update - Delete
//Método model() crea una clase, en este caso la clase ClienteModel, con los atributos de cliente + el esquema clienteSchema
const ClienteModel = model( "cliente" , clienteSchema )

module.exports = ClienteModel