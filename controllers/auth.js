const { request, response } = require("express");
const {compareSync} = require("bcryptjs")
const {sign} = require("jsonwebtoken")
const ClienteModel = require("../models/cliente");

async function login(req = request, res = response) {
    const {email, password} = req.body
    console.log (req.body)


    //Buscar cliente
    const cliente = await ClienteModel.findOne({email})

    if (cliente){
        try {
        //Validar constraseña
        //True: si las credenciales son válidas. La contraseña debe estar encriptada para comparación
        if(compareSync(password, cliente.password)){
            //Generar el token
            sign({id: cliente.id}, "grupo4-serviplus", (err, token)=>{

                if(err)
                    return res.status(500).send({mensaje: "Hubo un error"})
                else
                    return res.send({auth: true, token})
            })
        } else{
            return res.status(500).send({mensaje: "Contraseña incorrecta"})
        }
        } catch (error) {
            return res.status(500).send({mensaje: "Hubo un error", error})
        }
        
    } else {
        return res.status(400).send({mensaje: "No existe el usuario"})
    }

}

module.exports = {login}