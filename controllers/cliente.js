const { request, response } = require("express")
const {hashSync, genSaltSync} = require("bcryptjs")
const ClienteModel = require("../models/cliente");

//async para utilizar el await
async function crearCliente(req = request, res = response){
    //console.log(request.body);
    //Todo lo que reciba en el cuerpo de la petición, debe responder el mismo objeto: (visualizar respuesta en el postman)
    //response.send(request.body);

    //Para extraer un dato de la petición del Postman:
    const { numDocumento, password } = req.body

    //Para consultar si el cliente existe en la base de datos
    //await -> primero se ejecuta esta sentencia antes de la próxima sentencia
    const clienteEncontrado = await ClienteModel.findOne( {
        numDocumento: req.body.numDocumento} )

    //VALIDACIÓN
    //Si el cliente existe, responda como objeto "El cliente ya existe"
    if(clienteEncontrado){
        res.status(400).send({ mensaje: "El cliente ya existe"})
    } else {
        //Si el clinete no existe, lo creamos.
        //Primero se realiza Encriptación - password
        const passwordEncrypted = hashSync(password, genSaltSync())
        req.body.password = passwordEncrypted
        
        //Para crear un usuario (Promesa)
        ClienteModel.create(req.body)
        .then((clienteCreado)=>{
        res.status(201).send({mensaje: "El cliente fue creado con éxito", clienteCreado } )
    })
    .catch(()=>{
        res.send({mensaje: "Ocurrió un error al crear el cliente" })
    })                                                                                                                                                                                                     
    }
}

//Función (controlador) para consultar por el id
async function getClientes(req = request, res = response){
    const {id, email, numDocumento} = req.query // const {id, correo, docuemnto} = req.query -> Para utilizar más parámetros de consulta

    if(id || email || numDocumento ){
        //Para consultar por varios parámetros al mismo tiempo
        const cliente = await ClienteModel.findOne({$or: [{_id: id}, {email}, {numDocumento}]  })
        return res.send(cliente)
    } else {
        const listaClientes = await ClienteModel.find()
        res.send(listaClientes)
    }

    /* //Ya no es necesario el if, porque se implementó la consulta por varios parámetros 
    if (id){
        const cliente = await ClienteModel.findById(id)
        res.send(cliente)
    }else{
        const listaClientes = await ClienteModel.find()
        res.send(listaClientes)
    }
    */
}

//Función para mostrar información del usuario para el usuario
async function getCliente(req = request, res = response) {
    
    const {id} = req.params
    const cliente = await ClienteModel.findById( id )
    //const cliente = await ClienteModel.findOne({ email: id}) -> consulta por el email
    res.send(cliente)
}

//Función para modificar datos de un usuario
async function modificarCliente(req = request, res = response) {
    
    const {id, password, ...cliente} = req.body //... -> todos los datos restantes del body excepto el id

    console.log(cliente)
    
    cliente.password = hashSync(password, genSaltSync())

    //Utilizando findByIdAndUpdate -> const obj = await ClienteModel.findByIdAndUpdate(id, cliente)
    const obj = await ClienteModel.updateOne({_id:id}, cliente)

    res.send(obj)
}

//Función para eliminar datos de un usuario
async function borrarCliente(req = request, res = response) {
    
    const {id} = req.body
    const object = await ClienteModel.findByIdAndDelete(id)
    res.send(object)
}

module.exports = {crearCliente, getClientes, getCliente, modificarCliente, borrarCliente}
