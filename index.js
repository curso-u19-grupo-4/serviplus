//npm i mongoose -> instalar paquete mongoose y coloca la dependencia mongoose en package.json
//npm init -y -> iniciar proyecto con mongoose

//1. Conexión a la base de datos
//const mongoose = require("mongoose");
//const ClienteModel = require("./models/cliente");



//Se crea una carpeta config >> database
/*
//Método Connect -> resulta siendo una promesa
mongoose
  .connect("mongodb://localhost:27017/serviplus")//Para indicar a cuál BD nos debemos conectar: /serviplus
  .then( () => { console.log("Conexión hecha con éxito") })
  .catch( () => { console.log("Hubo un error en la conexión") })
*/


//Realizar todo en un solo script se vuelve insostenible, por lo tanto, se crea una carpeta models para la creación de los respectivos modelos
/*
//2. Definir un esquema (SCHEMA)
const cliente = {
    tipoDocumento: String,
    numDocumento: String, //Datos String pesan menos en la base de datos
    nombre: String,
    apellido: String,
    email: String,
    telefonoContacto: String,
    password: String
}

//Para decirle a Mongoose que hemos creado un modelo (o una plantilla):
const clienteSchema = mongoose.Schema(cliente)

//3. Definir el Modelo -> CRUD = Create - Read - Update - Delete
//Método model() crea una clase, en este caso la clase ClienteModel, con los atributos de cliente + el esquema clienteSchema
const ClienteModel = mongoose.model( "cliente" , clienteSchema )
*/


//Se crea una carpeta tests para las pruebas
/*
//Implementar el caso de uso:
//Información que se recopila a través del FrontEnd, por ejemplo, un formulario
const cliente1 = {
    tipoDocumento: "CC",
    numDocumento: "124",
    nombre: "PEPE2",
    apellido: "ALVAREZ",
    email: "pepealv@email.com",
    telefonoContacto: "3508024101",
    password: "0123"
}
//Los métodos CRUD se ejecutan como Promesas
ClienteModel.create(cliente1)
    .then( (respuesta) => {
        console.log(`Cliente ${respuesta.nombre} añadido con éxito`)
    } )
    .catch( () => {
        console.log(`Hubo un error al añadir al Cliente ${respuesta.nombre}`)
    } )
*/



/*
//Se crea un nuevo Script server.js
//Para EXPONER LOS SERVICIOS
const express = require("express")

const app = express() //Función express()
//Ahora podemos utilizar métodos de express()
//Segundo argumento es el controlador (generalmente es un callback (una función dentro de otra función))
app.get("/", (request, response) => {

    response.send("Hola, probando el nodemon")
})

app.post("/crear-usuario", (request, response) => {

    console.log(request.body)

    response.send("Hola, esta es la respuesta de response.send")
})

//Ahora debemos definir el puerto
app.listen(3000)
*/

const Server = require("./config/server")
const server = new Server()

