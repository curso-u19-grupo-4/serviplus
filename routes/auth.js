const { Router } = require("express");
const { login } = require("../controllers/auth");

const routerAuth = Router()

routerAuth.post("", login)

module.exports = routerAuth