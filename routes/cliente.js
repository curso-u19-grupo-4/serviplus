const { Router } = require('express')
const {crearCliente, getClientes, getCliente, modificarCliente, borrarCliente} = require("../controllers/cliente");
const routerCliente = Router()

//router.método.("/ruta", controlador)
routerCliente.post("/cliente", crearCliente)
routerCliente.get("/cliente", getClientes)
routerCliente.get("/cliente/:id", getCliente) //:id -> link dinámico
routerCliente.put("/cliente", modificarCliente)
routerCliente.delete("/cliente", borrarCliente)

module.exports = routerCliente