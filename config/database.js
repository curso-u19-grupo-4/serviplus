const mongoose = require("mongoose");

//Función que realiza la conexión
const conexionBD = () => {
  mongoose
    .connect("mongodb://localhost:27017/serviplus") //Para indicar a cuál BD nos debemos conectar: /serviplus
    .then(() => {
      console.log("Conexión hecha con éxito");
    })
    .catch(() => {
      console.log("Hubo un error en la conexión");
    });
};

module.exports = conexionBD;
