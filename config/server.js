const express = require("express");
const routerAuth = require("../routes/auth");
//const {crearCliente, getClientes, getCliente, modificarCliente, borrarCliente} = require("../controllers/cliente"); -> Ya no es necesario. Se implementó la carpeta Routes 
const routerCliente = require("../routes/cliente");
const conexionBD = require("./database");

class Server {
  //Crear atributos a través del constructor que es un método
  constructor() {
    this.port = 3000
    this.app = express();

    this.app.use(express.json());

    this.app.listen(this.port, () => {
      console.log("Se está ejecutando la APP")
    });

    this.routes();
    
    conexionBD()
  }

  routes() {
    /*
    this.app.get("/", (request, response) => {
      response.send("Hola, probando el nodemon");
    });
    */

    //Se crea carpeta Controllers para implementar las funciones de las rutas
    /*
    //RUTA PARA CREAR UN USUARIO:
    this.app.post("/usuario", (request, response) => {
      console.log(request.body);
      response.send(request.body);
    });
    */
    this.app.use( "/login", routerAuth)
    this.app.use( "/", routerCliente)
    

  }

}

module.exports = Server
