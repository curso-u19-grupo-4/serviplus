console.log("Hola desde el BackEnd"); //Para imprimir, en el terminal colocar -> 'node index' (index es el nombre del archivo a ejecutar)
//Para crear objetos utilizar 'const' (constante)
const obj = { nombre: "Jabón", cantidad: 10 }; //Creando un objeto con dos atributos
obj.id = "123"; //Adicionando un atributo a objeto existente
console.log(obj.cantidad);

//Las promesas (programación asíncrona)
//Función que se pasa como argumento a la Promesa

//Callback: Una función dentro de otra
//Creación de la promesa
const miPrimeraPromesa = new Promise((resolve, reject) => {
  const texto = "Hola desde el backend";
  const txt2 = "MISIONTIC"

  if (texto.match(txt2)) {
    resolve("Si existe la cadena en la constante", true);
  } else {
    reject(txt2);
  }
});

//Ejecución (llamado) de la promesa
//then() -> permite ejecutar la promesa. Trae los argumentos de resolve()
miPrimeraPromesa
  .then((respuesta) => {
    console.log(respuesta);
    //cath() -> lo que queremos hacer cuando se ejectura el reject()
  })
  .catch((error) => {console.log("No hay coincidencias de la palabra: " + error)});